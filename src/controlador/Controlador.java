package controlador;

import java.io.IOException;
import java.util.HashSet;
import modelo.Alumno;
import modelo.Modelo;
import modelo.ModeloArrayList;
import modelo.ModeloFichero;
import modelo.ModeloHashSet;
import modelo.ModeloVector;
import vista.Vista;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Controlador {

  private Modelo modelo;
  private Vista vista;
  private char opcion;

  Controlador(Modelo modelo_, Vista vista_) throws IOException {
    modelo = modelo_;
    vista = vista_;
    opcion = menumodelo();
    if (opcion != 'e') {
      menucrud();
    }
  }

  private void menucrud() throws IOException {
    String linea;
    char opcion = ' ';
    String id; // Posicion a actualziar// Posicion a actualizar
    Alumno alumno;

    do {

      opcion = vista.menu();

      opcion = aMinuscula(opcion);

      switch (opcion) {

        case 'e': // Exit
          vista.exit();
          break;

        case 'c': // Crear/AñadirAñadir
          alumno = vista.getAlumno();
          id = Integer.toString(modelo.getId() + 1);
          alumno.setId(id);
          modelo.create(alumno);

          break;
        case 'r': // Read / Obtener / Listar
            HashSet alumnos = new HashSet();
            alumnos =  modelo.read();
            vista.show(alumnos);
            
          break;
        case 'u':  // Actualizar   
          id = vista.getId();
          alumno = vista.getAlumno();
          alumno.setId(id);
          modelo.update(alumno);

          break;
        case 'd': // Borrar
          id = vista.getId();
          alumno = new Alumno(id, "", 0, "");
          modelo.delete(alumno);

          break;
        default:
          vista.error();
          break;
      }

    } while (opcion != 'e');

  }

  public static char aMinuscula(char opcion) {
    String linea;
    linea = opcion + "";
    linea = linea.toLowerCase();
    opcion = linea.charAt(0);
    return opcion;
  }

  private char menumodelo() throws IOException {

    String linea;
    char opcion = ' ';
    String id; // Posicion a actualziar// Posicion a actualizar
    Alumno alumno;

    opcion = vista.menuModelos();

    opcion = aMinuscula(opcion);

    switch (opcion) {

      case 'e': // Exit
        vista.exit();
        break;

      case 'v':
        modelo = new ModeloVector();
        break;
      case 'a':
        modelo = new ModeloArrayList();
        break;
      case 's':
        modelo = new ModeloHashSet();
        break;
      case 'f':
        modelo = new ModeloFichero();
        break;
      default:
        vista.error();
        break;
    }

    return opcion;

  }

}
