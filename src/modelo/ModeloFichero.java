package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.StringTokenizer;

/**
 *
 * @author paco
 */
public class ModeloFichero implements Modelo {

    private File fs;
    private int id = 0;
    private static String nombrefichero = "1415ceed1prgt6e1.cvs";

    private int calculaid() {
        int idmax = 0;

        if (fs.exists()) {

            try {

                FileReader fr = new FileReader(fs);
                BufferedReader br = new BufferedReader(fr);
                Alumno alumno;
                String linea;
                int id_;

                linea = br.readLine();
                while (linea != null) {
                    alumno = extraeAlumno(linea);
                    id_ = Integer.parseInt(alumno.getId());
                    if (id_ > idmax) {
                        idmax = id_;
                    }
                    linea = br.readLine();
                }

                fr.close();

            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }

        }
        return idmax;

    }

    public ModeloFichero() throws IOException {
        fs = new File(nombrefichero);
        id = calculaid();
    }

    @Override
    public void create(Alumno alumno) {

        FileWriter fw = null;
        try {
            fw = new FileWriter(fs, true);
            grabarAlumno(alumno, fw);
            fw.close();
        } catch (IOException ex) {
        }

        id++;

    }

    private Alumno extraeAlumno(String linea) {

        Alumno alumno;
        StringTokenizer str = new StringTokenizer(linea, ";");

        String id = str.nextToken();
        String nombre = str.nextToken();
        String edad_ = str.nextToken();
        int edad = Integer.parseInt(edad_);
        String email = str.nextToken();

        alumno = new Alumno(id, nombre, edad, email);
        return alumno;
    }

    @Override
    public HashSet read() {
        HashSet alumnos = new HashSet();
        try {
            FileReader fr = new FileReader(fs);
            BufferedReader br = new BufferedReader(fr);
            Alumno alumno;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                alumno = extraeAlumno(linea);
                alumnos.add(alumno);
                linea = br.readLine();
            }
            fr.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        return alumnos;
    }

    @Override
    public void update(Alumno alumno) {

        try {
            File temp = new File("temp.txt");
            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(fs);

            BufferedReader br = new BufferedReader(fr);
            Alumno a;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                a = extraeAlumno(linea);
                if (a.getId().equals(alumno.getId())) {
                    grabarAlumno(alumno, fw);
                } else {
                    grabarAlumno(a, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            fs.delete();
            temp.renameTo(fs);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }

    @Override
    public void delete(Alumno alumno) {

        try {

            File temp = new File("temp.txt");

            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(fs);

            BufferedReader br = new BufferedReader(fr);
            Alumno a;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                a = extraeAlumno(linea);
                if (!a.getId().equals(alumno.getId())) {
                    grabarAlumno(a, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            fs.delete();
            temp.renameTo(fs);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }

    @Override
    public int getId() {
        return id;
    }

    private void grabarAlumno(Alumno alumno, FileWriter fw) throws IOException {
        fw.write(alumno.getId());
        fw.write(";");
        fw.write(alumno.getNombre());
        fw.write(";");
        fw.write(alumno.getEdad() + "");
        fw.write(";");
        fw.write(alumno.getEmail());
        fw.write("\r\n");
    }

}
