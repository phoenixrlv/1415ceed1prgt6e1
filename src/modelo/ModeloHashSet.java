/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.HashSet;
import java.util.Iterator;


/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ModeloHashSet implements Modelo {

  HashSet alumnos = new HashSet();
  int id = 0;

  @Override
  public void create(Alumno alumno) {
    alumnos.add(alumno);
    id++;
  }

  @Override
    public HashSet read() {
    return alumnos;
  }

  public void update(Alumno alumno) {

    Iterator it = alumnos.iterator();
    Alumno a;
    int pos = 0;

    while (it.hasNext()) {
      a = (Alumno) it.next();
      if (a.getId().equals(alumno.getId())) {
        alumnos.remove(a);
        alumnos.add(alumno);
      }
    }
  }

  public void delete(Alumno alumno) {

    Iterator it = alumnos.iterator();
    Alumno a;
    int pos = 0;

    while (it.hasNext()) {
      a = (Alumno) it.next();
      if (a.getId().equals(alumno.getId())) {
        alumnos.remove(a);

      }
    }

  }

  @Override
  public int getId() {
    return id;
  }

}
